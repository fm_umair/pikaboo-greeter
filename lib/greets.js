const greets = [
  'Hola',
  'Bonjour',
  'Salam',
  'Namasty',
  'Pkhair Raglay',
  'Gee Aayan Nu'
];

const randomIndexGenerator = () =>  Math.floor(Math.random(4) * 10000 % 4);

module.exports = {
  greet: greets[randomIndexGenerator()]
};
